<?php use System\Route;

Route::set('',       'IndexController@index');
Route::set('create', 'TaskController@index');
Route::set('auth',   'AuthController@index');
Route::set('edit',   'TaskController@edit');

Route::set('ajaxCreate', 'TaskController@ajaxCreate');
Route::set('ajaxDelete', 'TaskController@ajaxDelete');
Route::set('ajaxCheck',  'TaskController@ajaxCheck');
Route::set('ajaxEdit',   'TaskController@ajaxEdit');

Route::set('ajaxLogin',  'AuthController@ajaxLogin');
Route::set('ajaxLogout', 'AuthController@ajaxLogout');
