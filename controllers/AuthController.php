<?php namespace Controllers;

use Models\Users;
use Rakit\Validation\Validator;
use System\Auth as Authentification;

class AuthController
{
    public function index()
    {
        return view('auth');
    }

    public function ajaxLogin()
    {
        if (Authentification::check()) {
            die(toJson('Вы уже авторизированы.'));
        }

        $data = post();

        $validator = new Validator;
        $validation = $validator->make($data, [
            'user' => 'required|between:3,32',
            'password' => 'required|between:3,255'
        ]);
        $validation->validate();
        if ($validation->fails()) {
            $errors = $validation->errors();
            die(toJson($errors->all()[0]));
        }

        extract($data);
        $password = md5($password);

        $userFromDb = Users::select('*', ["user = \"{$user}\"", "password = \"{$password}\""]);
        if ($userFromDb) {
            echo toJson('', true);
            return Authentification::login($user);
        }

        die(toJson('Пользователь с такими данными не найден.'));
    }

    public function ajaxLogout()
    {
        if (!Authentification::check()) {
            die(toJson('Вы не авторизированы.'));
        }

        echo toJson('', true);
        return Authentification::logout();
    }

    /**
     * @method toJson
     * @param  string  $message
     * @param  boolean $success
     * @return string
     */
    private function toJson(string $message, $success = false): string
    {
        return json_encode([
            'success' => $success,
            'message' => $message
        ]);
    }
}
