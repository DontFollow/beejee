<?php namespace Controllers;

use Models\Tasks;

class IndexController
{
    private $page;
    private $order;

    public function __construct()
    {
        $this->page  = get('page') ?: 1;
        $this->order = get('order') ?: 'id';
    }

    public function index()
    {
        $tasks = Tasks::instance($this->order, ($this->order == 'id') ? 'DESC' : 'ASC');
        return view('index', ['tasks' => $tasks]);
    }
}
