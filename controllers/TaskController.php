<?php namespace Controllers;

use Exception;
use System\Auth;
use Models\Tasks;
use Rakit\Validation\Validator;

class TaskController
{
    public function index()
    {
        return view('create');
    }

    public function edit()
    {
        $id = get('id');
        $task = Tasks::select('*', ["id = {$id}"]);
        return $task ? view('edit', ['task' => $task[0]]) : false;
    }

    public function ajaxCreate()
    {
        $data = post();

        $this->validateCreateOrEdit($data);

        Tasks::insert($data);

        die(toJson('', true));
    }

    public function ajaxDelete()
    {
        if (!(Auth::check() && Auth::isAdmin())) {
            return false;
        }

        $data = post();

        $this->validateDeleteOrCheck($data);

        extract($data);

        $task = Tasks::select('*', ["id = {$id}"]);
        if ($task) {
            Tasks::delete(["id = {$id}"]);
            die(toJson('', true));
        }

        die(toJson('Данное задание не было найдено.'));
    }

    public function ajaxCheck()
    {
        if (!(Auth::check() && Auth::isAdmin())) {
            return false;
        }

        $data = post();

        $this->validateDeleteOrCheck($data);

        extract($data);

        Tasks::update(["id = {$id}"], ['done = 1']);

        die(toJson('', true));
    }

    public function ajaxEdit()
    {
        if (!(Auth::check() && Auth::isAdmin())) {
            return false;
        }

        $data = post();

        $this->validateCreateOrEdit($data);

        extract($data);

        Tasks::update([
            "creator = {$creator}",
            "email = {$email}",
            "title = {$title}",
            "task = {$task}"
        ], [
            "id = {$id}"
        ]);

        die(toJson('', true));
    }

    private function validateDeleteOrCheck(array $data)
    {
        $validator = new Validator;
        $validation = $validator->make($data, [
            'id' => 'required'
        ]);
        $validation->validate();
        if ($validation->fails()) {
            $errors = $validation->errors();
            die(toJson($errors->all()[0]));
        }
    }

    private function validateCreateOrEdit(array $data)
    {
        $validator = new Validator;
        $validation = $validator->make($data, [
            'creator' => 'required|between:3,32',
            'email'   => 'required|email',
            'title'   => 'required|between:3,255',
            'task'    => 'required|min:3'
        ]);
        $validation->validate();
        if ($validation->fails()) {
            $errors = $validation->errors();
            die(toJson($errors->all()[0]));
        }
    }
}
