<?php

/**
 * Task book application.
 * The test task for BeeJee company.
 *
 * @author  Vitaly Eksuzian
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);

session_start();

/**
 * Register The Composer Auto Loader.
 */
require __DIR__ . '/vendor/autoload.php';
