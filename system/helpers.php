<?php

if (!function_exists('base_dir')) {

    function base_dir() {
        return __DIR__ . '/..';
    }

}

if (!function_exists('config')) {

    function config($direction) {
        return System\Config::get($direction);
    }

}

if (!function_exists('get')) {

    function get($param = null) {
        if (is_null($param)) {
            return $_GET;
        }
        return @$_GET[$param] ?: false;
    }

}

if (!function_exists('post')) {

    function post($param = null) {
        if (is_null($param)) {
            return $_POST;
        }
        return @$_POST[$param] ?: false;
    }

}

if (!function_exists('input')) {

    function input($param = null) {
        if (is_null($param)) {
            return $_REQUEST;
        }
        return @$_REQUEST[$param] ?: false;
    }

}

if (!function_exists('auth')) {

    function auth() {
        return System\Auth::check();
    }

}

if (!function_exists('view')) {

    function view($name, $args = []) {
        $viewDirection = base_dir() . "/views/{$name}.php";
        if (file_exists($viewDirection)) {
            $view = file_get_contents($viewDirection);
            $twigLoader = new Twig\Loader\ArrayLoader(['view' => $view]);
            $args['page']    = @get('page') ?: 1;
            $args['auth']    = @auth();
            $args['user']    = @System\Auth::getUser();
            $args['isAdmin'] = @System\Auth::isAdmin();
            echo (new Twig\Environment($twigLoader))->render('view', $args);
        }
    }

}

if (!function_exists('toJson')) {

    function toJson($message, $success = false) {
        return json_encode([
            'success' => $success,
            'message' => $message
        ]);
    }

}
