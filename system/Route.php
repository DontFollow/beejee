<?php namespace System;

/**
 * Class Route needs to register routes.
 */
class Route
{
    /**
     * Method creates valid route.
     *
     * @method set
     * @param  string $route
     * @param  string $handler
     */
    public static function set(string $route, string $handler): void
    {
        list($controller, $method) = explode('@', $handler);

        if ($route === '') $route = 'index.php';
        if (isset($_GET['url']) && $_GET['url'] === $route) {
            unset($_GET['url']);
            $controller = "Controllers\\{$controller}";
            call_user_func([new $controller, $method]);
        }
    }

    /**
     * Method returns all registred routes.
     *
     * @method getRoutes
     * @return array
     */
    public static function getRoutes(): array
    {
        return self::$routes;
    }
}
