<?php namespace System;

use PDO;
use PDOException;

class Model
{
    protected static $connection = false;

    protected static function connect()
    {
        $databaseConfig = config('database.mysql');

        if (!self::$connection) {
            try {
                $dbh = "mysql:host={$databaseConfig['host']};dbname={$databaseConfig['name']}";
                self::$connection = new PDO($dbh, $databaseConfig['user'], $databaseConfig['pass']);
                self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }

        return self::$connection;
    }

    public static function select($what, $where)
    {
        $table = static::$table;

        $wheres = implode(' and ', $where);

        $query = "SELECT {$what} FROM `{$table}` WHERE {$wheres}";

        $statement = self::connect()->prepare($query);
        $statement->execute();
        return $statement->fetchAll();
    }

    public static function instance(string $orderBy = 'id', string $order = 'ASC')
    {
        $table = static::$table;

        $query = "SELECT * FROM `{$table}` ORDER BY `{$orderBy}` {$order}";

        $statement = self::connect()->prepare($query);
        $statement->execute();
        return $statement->fetchAll();
    }

    public static function insert(array $array)
    {
        $table = static::$table;

        $columns = array_keys($array);
        $values  = array_values($array);

        $query = "INSERT INTO `{$table}` (%c) VALUES (%v)";

        $query = str_replace('%c', self::toString($columns), $query);
        $query = str_replace('%v', self::toStringQuote($values), $query);

        $statement = self::connect()->prepare($query);
        return $statement->execute();
    }

    public static function update(array $where, array $update)
    {
        $table   = static::$table;

        $wheres  = implode(' and ', $where);

        $updates = '';
        for ($i = 0; $i < count($update); $i++) {
            $updatePieces = explode(' = ', $update[$i]);
            $updates .= "{$updatePieces[0]} = " . self::toQuote($updatePieces[1]);
            $updates .= ($i + 1 == count($update)) ? ' ' : ', ';
        }

        $query = "UPDATE {$table} SET {$updates} WHERE {$wheres}";

        $statement = self::connect()->prepare($query);
        return $statement->execute();
    }

    public static function delete(array $array)
    {
        $table = static::$table;

        $wheres = implode(' and ', $array);

        $query = "DELETE FROM `{$table}` WHERE {$wheres}";

        $statement = self::connect()->prepare($query);
        return $statement->execute();
    }

    private static function toQuote(string $string)
    {
        return self::connect()->quote($string);
    }

    private static function toStringQuote(array $array)
    {
        $newArray = [];
        foreach ($array as $value) {
            array_push($newArray, self::connect()->quote($value));
        }
        return self::toString($newArray);
    }

    private static function toString(array $array)
    {
        return implode(', ', $array);
    }
}
