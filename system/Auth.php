<?php namespace System;

class Auth
{
    public static function check()
    {
        return self::getUser() ? true : false;
    }

    public static function getUser()
    {
        return @$_SESSION['user'] ?: false;
    }

    public static function login($user): void
    {
        if (!Auth::check()) {
            $_SESSION['user'] = $user;
        }
    }

    public static function logout(): void
    {
        if (isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }
    }

    public static function isAdmin()
    {
        return self::getUser() === 'admin';
    }
}
