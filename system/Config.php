<?php namespace System;

/**
 * Class gets configs from config path.
 */
class Config
{
    /**
     * @method get
     * @param string $direction
     */
    public static function get(string $direction)
    {
        $needles = explode('.', $direction);
        $configName = $needles[0];
        unset($needles[0]);

        $config = require base_dir() . "/config/{$configName}.php";

        foreach ($needles as $needle) {
            if (!isset($config[$needle])) {
                return false;
            }
            $config = $config[$needle];
        }

        return $config;
    }
}
