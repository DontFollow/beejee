<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>BeeJee</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="/">BeeJee Task</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Авторизация</a>
                    </li>
                </ul>
                {% if auth %}
                    <span class="m-3">Привет, {{ user }}!</span>
                {% endif %}
                <a class="btn btn-outline-success my-2 my-sm-0 m-1" href="/create">Создать задание</a>
                {% if auth %}
                    <a href="#" id="logout" class="btn btn-outline-danger my-2 my-sm-0 m-1">Выйти</a>
                {% endif %}
            </div>
        </nav>

        <div class="container mt-5">
            <div class="row">
                {% if not auth %}
                    <form class="col-md-12" id="auth" action="/ajaxLogin">
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="user">Логин</label>
                                <input name="user" type="text" class="form-control" id="user" placeholder="admin" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                <label for="title">Пароль</label>
                                <input name="password" type="password" class="form-control" id="password" placeholder="123" required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" id="login">Войти</button>
                    </form>
                {% else %}
                    <h2>Вы уже авторизованы</h2>
                {% endif %}
            </div>
        </div>

        <footer>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        </footer>

        <script type="text/javascript">
            $("form#auth").on('submit', function () {
                event.preventDefault();
                $.ajax({
                    url: "/ajaxLogin",
                    data: $(this).serialize(),
                    method: 'POST',
                    success: function (data) {
                        data = JSON.parse(data);
                        if (data['success']) {
                            return location.replace("https://dontfollow.ru");
                        }
                        alert(data['message']);
                    }
                });
            });
        </script>

        <script type="text/javascript">
            $("a#logout").click(function () {
                event.preventDefault();
                $.ajax({
                    url: "/ajaxLogout",
                    data: [],
                    method: "POST",
                    success: function (data) {
                        data = JSON.parse(data);
                        if (data['success']) {
                            return location.reload();
                        }
                        alert(data['message']);
                    }
                });
            });
        </script>
        <script type="text/javascript">
            $("a#logout").click(function () {
                $.ajax({
                    url: "/ajaxLogout",
                    data: [],
                    method: "POST",
                    success: function (data) {
                        data = JSON.parse(data);
                        if (data['success']) {
                            return location.reload();
                        }
                        alert(data['message']);
                    }
                });
            });
        </script>
    </body>
</html>
