<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>BeeJee</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <script type="text/javascript">
            $(function () { $('[data-toggle="tooltip"]').tooltip() })
        </script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="/">BeeJee Task</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Список заданий</a>
                    </li>
                </ul>
                {% if auth %}
                    <span class="m-3">Привет, {{ user }}!</span>
                {% endif %}
                <a class="btn btn-outline-success my-2 my-sm-0 m-1" href="/create">Создать задание</a>
                {% if not auth %}
                    <a class="btn btn-outline-danger my-2 my-sm-0 m-1" href="/auth">Войти</a>
                {% else %}
                    <a href="#" id="logout" class="btn btn-outline-danger my-2 my-sm-0 m-1">Выйти</a>
                {% endif %}
            </div>
        </nav>

        <div class="container mt-5">
            <div class="row">
                {% if tasks %}
                    <div class="col-md-12 m-2 text-center">
                        <a class="btn" href="/?order=creator">Сортировать по имени</a>
                        <a class="btn" href="/?order=email">Сортировать по Email</a>
                        <a class="btn" href="/?order=done">Сортировать по статусу</a>
                    </div>
                    {% for i in 0..2 if tasks[3*page-3+i] is defined %}
                        {% set index = 3 * page - 3 + i %}
                        <div class="col-md-12 m-2">
                            <div class="card text-center">
                                <div class="card-header">
                                    Автор: <b>{{ tasks[index].creator }}</b><br>Email: <b>{{ tasks[index].email }}</b>
                                    <a class="float-right ml-2 text-warning" data-toggle="tooltip" data-placement="top" title="Редактировать задание" href="/edit?id={{ tasks[index].id }}"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                    {% if tasks[index].done %}
                                        <span data-toggle="tooltip" data-placement="top" title="Задание выполнено" style="float: right; color: green"><i class="fa fa-check" aria-hidden="true"></i></span>
                                    {% elseif isAdmin %}
                                        <a href="#" id="done_{{ tasks[index].id }}" data-toggle="tooltip" data-placement="top" title="Сделать задание выполненным" style="float: right"><i class="fa fa-check" aria-hidden="true"></i></a>
                                    {% endif %}
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title">{{ tasks[index].title }}</h5>
                                    <p class="card-text">{{ tasks[index].task }}</p>
                                </div>
                                <div class="card-footer">
                                    {% if not isAdmin and not tasks[index].done %}
                                        <a href="#" class="btn btn-primary">Приступить к выполнению</a>
                                    {% elseif isAdmin %}
                                        <form id="update_{{ tasks[index].id }}">
                                            <input type="hidden" name="id" value="{{ tasks[index].id }}">
                                        </form>
                                        <a style="color: white" class="btn btn-danger" id="delete_{{ tasks[index].id }}">Удалить задание</a>
                                    {% endif %}
                                    
                                </div>
                            </div>
                        </div>
    
                        <script type="text/javascript">
                            $("a#delete_{{ tasks[index].id }}").click(function () {
                                event.preventDefault();
                                $.ajax({
                                    url: "/ajaxDelete",
                                    data: $("form#update_{{ tasks[index].id }}").serialize(),
                                    method: 'POST',
                                    success: function (data) {
                                        data = JSON.parse(data);
                                        if (data['success']) {
                                            return location.reload();
                                        }
                                        alert(data['message']);
                                    }
                                });
                            });
                        </script>
                        <script type="text/javascript">
                            $("a#done_{{ tasks[index].id }}").click(function () {
                                $.ajax({
                                    url: "/ajaxCheck",
                                    data: $("form#update_{{ tasks[index].id }}").serialize(),
                                    method: 'POST',
                                    success: function (data) {
                                        data = JSON.parse(data);
                                        if (data['success']) {
                                            return location.reload();
                                        }
                                        alert(data['message']);
                                    }
                                });
                            });
                        </script>
                    {% endfor %}
                
                    <div class="col-md-12">
                        <ul class="pagination justify-content-center">
                            <li class="page-item {% if page == 1 %}disabled{% endif %}">
                                <a class="page-link" href="/?page={{ page - 1 }}"><</a>
                            </li>
    
                            {% set tasksLength = tasks|length %}
                            {% set scope = tasksLength / 3 %}
                            {% set scope = scope|round(0, 'ceil') %}
    
                            {% for i in range(1, scope) %}
                                <li class="page-item"><a class="page-link" href="/?page={{ i }}">{{ i }}</a></li>
                            {% endfor %}
                            <li class="page-item {% if scope == page %}disabled{% endif %}">
                                <a class="page-link" href="/?page={{ page + 1 }}">></a>
                            </li>
                        </ul>
                    </div>
                {% else %}
                    <div class="col-md-12">
                        <h4 class="text-center">Задач нет...</h4>
                    </div>
                {% endif %}
            </div>
        </div>

        <footer>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        </footer>

        <script type="text/javascript">
            $("a#logout").click(function () {
                $.ajax({
                    url: "/ajaxLogout",
                    data: [],
                    method: "POST",
                    success: function (data) {
                        data = JSON.parse(data);
                        if (data['success']) {
                            return location.reload();
                        }
                        alert(data['message']);
                    }
                });
            });
        </script>
    </body>
</html>
